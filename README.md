# fcc-weather-app
Submission for [Free Code Camp's][1] [Weather API Project][2].

## Live Demos

[https://larrytooley.github.io/fcc-weather-app/][3]

[https://weathertron.surge.sh][13]

## Objective

Build a [CodePen.io][4] app that is functionally similar to this: [http://codepen.io/FreeCodeCamp/full/bELRjV][5].

## USER STORIES

* I CAN SEE THE WEATHER IN MY CURRENT LOCATION.

* I CAN SEE A DIFFERENT ICON OR BACKGROUND IMAGE (E.G. SNOWY MOUNTAIN, HOT DESERT) DEPENDING ON THE WEATHER.

* I CAN PUSH A BUTTON TO TOGGLE BETWEEN FAHRENHEIT AND CELSIUS.

## DEPLOYMENT

### API Keys

This app requires two third party api keys. One for open weather map and the other for the google reverse geocode. Both are available for free and you can visit the links below to find out how to obtain api keys.

**PLEASE DO NOT USE THE KEYS IN THIS EXAMPLE CODE**.

To deploy this app, download the code in a ZIP file or clone the repository. Update the `keys.js` file with your API keys.

### Deploying With Surge

Surge is a static page publishing service. Depending on your requirements it can be free to use.

* To get started you must have [Node.js][6] installed. Visit the link to find instructions for your operating system.

* Next run the commands `npm install -g surge` and `surge`.

* Follow the prompts to upload your project. Find more at [https://surge.sh][7]

* Once complete, your app will be accesible at [https://<app_name>.surge.sh][].


## Built With

* [jQuery][8]
* [https://darkskyapp.github.io/skycons/][9]
* [http://openweathermap.org/api][10]
* [Google Reverse Geocode][11]
* [CORS Anywhere][12]



[1]: https://www.freecodecamp.org/

[2]: https://www.freecodecamp.com/challenges/show-the-local-weather

[3]: https://larrytooley.github.io/fcc-weather-app/

[4]: https://codepen.io

[5]: http://codepen.io/FreeCodeCamp/full/bELRjV

[6]: http://nodejs.org/

[7]: https://surge.sh

[8]: https://jquery.com/

[9]: https://darkskyapp.github.io/skycons/

[10]: http://openweathermap.org/api

[11]: https://developers.google.com/maps/documentation/javascript/examples/geocoding-reverse

[12]: https://cors-anywhere.herokuapp.com/

[13]: https://weathertron.surge.sh
