'use strict'

// API key
var apiKey = KEYS.WEATHER;
var googleApiKey = KEYS.REVERSE_GEOCODE;

// Copyright Date Script
var today = new Date();
var year = today.getFullYear();
var copyright = '&copy; ' + year + ' Larry Tooley';

document.getElementById('copyright').innerHTML = copyright;


//Day and Time
var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
var now = new Date();
var day = days[now.getDay()];

var minutes;
if (now.getMinutes() < 10) {
    minutes = '0' + now.getMinutes();
} else {
    minutes = now.getMinutes();
}

var amPM;
var hour;
if (now.getHours() === 0) {
    hour = now.getHours() + 12;
    amPM = 'AM';
} else if (now.getHours() === 12) {
    hour = now.getHours();
    amPM = 'PM';
} else if (now.getHours() > 12) {
    hour = now.getHours() - 12;
    amPM = 'PM';
} else {
    hour = now.getHours();
    amPM = 'AM';
}

var dayTime = day + ' ' + hour + ':' + minutes + ' ' + amPM
document.getElementById('day-time').innerHTML = dayTime;

// skycons.js
var icons = new Skycons({'color': 'white'});
icons.set("condition", 'CLEAR_DAY');
icons.play();

// Weather Conditions

var weatherIcons = {
    '01d.png': {'skycon': 'CLEAR_DAY'},
    '01n.png': {'skycon': 'CLEAR_NIGHT'},
    '02d.png': {'skycon': 'PARTLY_CLOUDY_DAY'},
    '02n.png': {'skycon': 'PARTLY_CLOUDY_NIGHT'},
    '03d.png': {'skycon': 'CLOUDY'},
    '03n.png': {'skycon': 'CLOUDY'},
    '04d.png': {'skycon': 'CLOUDY'},
    '04n.png': {'skycon': 'CLOUDY'},
    '09d.png': {'skycon': 'RAIN'},
    '09n.png': {'skycon': 'RAIN'},
    '10d.png': {'skycon': 'RAIN'},
    '10n.png': {'skycon': 'RAIN'},
    '11d.png': {'skycon': 'RAIN'},
    '11n.png': {'skycon': 'RAIN'},
    '13d.png': {'skycon': 'SNOW'},
    '13n.png': {'skycon': 'SNOW'},
    '50d.png': {'skycon': 'FOG'},
    '50n.png': {'skycon': 'FOG'}
};


$(document).ready(function () {

    navigator.geolocation.getCurrentPosition(function (position) {
        var lat = position.coords.latitude;
        var lon = position.coords.longitude;

        var api = 'https://cors-anywhere.herokuapp.com/'
                  + 'http://api.openweathermap.org/data/2.5/find?'
                  + 'lat=' + lat
                  + '&lon=' + lon
                  + '&cnt=1&units=imperial&appid='
                  + apiKey;


        $.getJSON(api, function (data) {

            var fahr = Math.round(data.list[0].main.temp);
            var cel = Math.round((fahr - 32) * 5 / 9);
            var tempUnits = 'f';
            var temp = fahr;
            var units = '&#8457;';

            document.querySelector('#temp').innerText = temp;
            document.querySelector('#units').innerHTML = units;

            var icon = data.list[0].weather[0].icon;
            var iconImage = icon + '.png';
            var weatherIcon = weatherIcons[iconImage].skycon;
            icons.set("condition", weatherIcon);
            icons.play(); 
            var location = data.list[0].name;
            document.querySelector('#location').innerText = location;


            var celButton = document.querySelector('#cel');
            var fahButton = document.querySelector('#fah');
            var tempDisplay = document.querySelector('#tempDisplay');

            celButton.addEventListener('click', function() {
                if (tempUnits === 'f') {
                    tempUnits = 'c';
                    units = '&#8451;'
                    temp = cel;
                    document.querySelector('#temp').innerText = temp;
                    document.querySelector('#units').innerHTML = units;
                    document.querySelector('#cel').style.background = 'blue';
                    document.querySelector('#fah').style.background = 'gray';
                }
            });

            fahButton.addEventListener('click', function() {
                if (tempUnits === 'c') {
                    tempUnits = 'f';
                    units = '&#8457;';
                    temp = fahr;
                    document.querySelector('#temp').innerText = temp;
                    document.querySelector('#units').innerHTML = units;
                    document.querySelector('#cel').style.background = 'gray';
                    document.querySelector('#fah').style.background = 'blue';
                }
            });
        });

        var locationApi = 'https://maps.googleapis.com/maps/api/geocode/'
        + 'json?latlng='
        + lat
        +','
        + lon
        + '&key='
        + googleApiKey;

        $.getJSON(locationApi, function(data) {
            var city = data.results[0].address_components[2].long_name;
            var state = data.results[0].address_components[5].short_name;
            document.querySelector('#location').innerText = city + ', ' + state;
        });
    });
});


